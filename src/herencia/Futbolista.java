/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

import java.util.Scanner;

/**
 *
 * @author Sileny Araya
 */
public class Futbolista extends SeleccionFutbol{
    
       public int dorsal;
    public String demarcacion;

    public Futbolista(int id, String nombre,String apellidos, int edad, int dorsal,String demarcacion) {
        super(id, nombre, apellidos, edad);
        this.dorsal = dorsal;
        this.demarcacion = demarcacion;
    }
    
     public String getAtributos() {
        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nApellido:" + apellidos
                + "\nEdad: " + edad
                + "\nDorsal: " + dorsal
                + "\nDemarcacion: " + demarcacion;
    }
     
     public void jugarPartido (){
         System.out.println("El futbolista "+ nombre+ " jugara un partido la proxima semana");
         SeleccionFutbol s =new SeleccionFutbol(0, " ", " ", 0);
         s.tipoPersona();
     }
     public void entrenar (){
         System.out.println("Al futbolista "+ nombre+ " le toca entrenar mañana");
     }

    //polimorfismo
    public void tipoPersona() {
        System.out.println("la persona es futbolista");
    }
    
    public void datosFut (){
        Scanner sc = new Scanner(System.in);
            System.out.println("\nIntroduzca el id del futbolista: ");
            id = sc.nextInt();
            System.out.println("Introduzca el nombre del futbolista: ");
            nombre = sc.next();
            System.out.println("Introduzca el apellido del futbolista: ");
            apellidos = sc.next();
            System.out.println("Introduzca la edad del futbolista: ");
            edad = sc.nextInt();
            System.out.println("Introduzca el dorsal del futbolista: ");
            dorsal = sc.nextInt();
            System.out.println("Introduzca la deamrcacion del futbolista: ");
            demarcacion = sc.next();
            
            System.out.println("\nFutbolista ingresado correctamente ");
            
    }
    
    }
    

