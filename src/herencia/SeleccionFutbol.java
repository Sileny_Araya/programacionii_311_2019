/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

import java.util.Scanner;

/**
 *
 * @author Sileny Araya
 */
public class SeleccionFutbol {

    public String nombre, apellidos;
    public int id, edad;

    public SeleccionFutbol(int id, String nombre, String apellidos, int edad) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;

    }

    public String getAtributos() {
        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nApellidos:" + apellidos
                + "\nEdad: " + edad;
    }

    public void tipoPersona() {

        Scanner ingresar = new Scanner(System.in);
        while (true) {
            System.out.println("\n1.Ingresar futbolista\n2.Ingresar entrenador\n3.Ingresar masajista\n4.Salir\n"
                    + "\nSeleccione una opción: ");
            int opcion = ingresar.nextInt();
            switch (opcion) {
                case 1:
                    Futbolista fut = new Futbolista(0, " ", " ", 0, 0, " ");
                    fut.datosFut();
                    fut.tipoPersona();
                    fut.entrenar();
                    fut.jugarPartido();

                case 2:
                    Entrenador ent = new Entrenador(0, " ", " ", 0, " ");
                    ent.datosEntre();
                    ent.tipoPersona();
                    ent.dirigirEntrenamiento();
                    ent.dirigirPartido();
                case 3:
                    Masajista mas = new Masajista(0, " ", " ", 0, " ", 0);
                    mas.datosMasi();
                    mas.tipoPersona();
                    mas.darMasaje();

                case 4:
                    System.exit(0);

                default:
                    System.out.println("Error! Digite opción válida!!");
                    break;
            }
        }

    }
}
