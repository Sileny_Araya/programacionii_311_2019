/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practica1;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author Sileny Araya
 */
public class LogicaNumero {

    int numSecreto, numIntentos = 3, numIntroducido, intentos = 3;
    boolean Acertar = false;

    public void Adivinador() {

        numSecreto = (int) (Math.random() * 10 + 1);
        for (int i = 1; i <= intentos && Acertar == false; i++) {
            Scanner sc = new Scanner(System.in);
            System.out.println("\nIntroduce un numero del 1 al 10: ");
            numIntroducido = sc.nextInt();

            if (numSecreto == numIntroducido) {
                Acertar = true;
            } else {
                if (numIntroducido > numSecreto) {
                    numIntentos = numIntentos - 1;
                    System.out.println(" El numero tiene que ser menor, te quedan " + numIntentos + " intentos");
                } else {
                    numIntentos = numIntentos - 1;
                    System.out.println("El numero introducido es menor, te quedan " + numIntentos + " intentos ");
                }
            }
        }
        if (Acertar == true) {
            System.out.println("Felicidades, has acertado el numero correcto");
            MenuPrincipal mp = new MenuPrincipal();
            mp.Inicio();
        } else {
            System.out.println("Has alcanzado el máximo de intentos");
            System.out.println("El numero secreto era: " + numSecreto);
            MenuPrincipal mp = new MenuPrincipal();
            mp.Inicio();
            ;
        }
    }
}
