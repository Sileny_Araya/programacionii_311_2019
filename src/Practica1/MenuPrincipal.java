/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practica1;

import java.util.Scanner;

/**
 *
 * @author Sileny Araya
 */
public class MenuPrincipal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Inicio(); 
        
    }
    
    public static void Inicio(){
        Scanner ingresar = new Scanner(System.in);
        while (true) {
            System.out.println("\n1.Logica Numero\n2.Logica Matriz\n3.Logica Palabra\n4.Salir\n"
                    + "\nSeleccione una opción: ");
            int opcion = ingresar.nextInt();
            switch (opcion) {
                case 1:
                    LogicaNumero ln = new LogicaNumero();
                    ln.Adivinador();
                    opcion = ingresar.nextInt();
                case 2:
                    Matriz m = new Matriz();
                    m.LogicaEdades();
                case 3:
                    LogicaPalabras rp = new LogicaPalabras();
                    rp.RimaPalabras();
                case 4:
                    System.exit(0);

                default:
                    System.out.println("Error! Digite opción válida!!");
                    break;
            }
        }
    }
}
